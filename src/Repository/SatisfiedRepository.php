<?php

namespace App\Repository;

use App\Entity\Satisfied;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Satisfied|null find($id, $lockMode = null, $lockVersion = null)
 * @method Satisfied|null findOneBy(array $criteria, array $orderBy = null)
 * @method Satisfied[]    findAll()
 * @method Satisfied[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SatisfiedRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Satisfied::class);
    }

    /**
     * @param $user
     * @param $post
     * @return Satisfied[] Returns an array of LikeDislike objects
     */
    public function findByUserField($user, $post)
    {
        try {
            return $this->createQueryBuilder('l')
                ->andWhere('l.user_id = :val')
                ->setParameter('val', $user)
                ->andWhere('l.post_id = :post')
                ->setParameter('post', $post)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        };
    }

    /**
     * @param $value
     * @return mixed
     */
    public function countField($value)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('sum(a.satisfaction)')
                ->andWhere('a.post_id = :val')
                ->setParameter('val', $value)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        };
    }
}
