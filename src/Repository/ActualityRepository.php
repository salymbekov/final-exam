<?php

namespace App\Repository;

use App\Entity\Actuality;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Actuality|null find($id, $lockMode = null, $lockVersion = null)
 * @method Actuality|null findOneBy(array $criteria, array $orderBy = null)
 * @method Actuality[]    findAll()
 * @method Actuality[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActualityRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Actuality::class);
    }

    /**
     * @param $user
     * @param $post
     * @return Actuality[] Returns an array of LikeDislike objects
     */
    public function findByUserField($user, $post)
    {
        try {
            return $this->createQueryBuilder('l')
                ->andWhere('l.user_id = :val')
                ->setParameter('val', $user)
                ->andWhere('l.post_id = :post')
                ->setParameter('post', $post)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        };
    }


    /**
     * @param $value
     * @return mixed
     */
    public function countField($value)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('sum(a.actual)')
                ->andWhere('a.post_id = :val')
                ->setParameter('val', $value)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        };
    }

}
