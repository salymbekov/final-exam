<?php

namespace App\Repository;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Post|null find($id, $lockMode = null, $lockVersion = null)
 * @method Post|null findOneBy(array $criteria, array $orderBy = null)
 * @method Post[]    findAll()
 * @method Post[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }

//    /**
//     *
//     * @param int $offset
//     * @param int $limit
//     * @return mixed
//     */
//    public function postForPagination($offset, $limit)
//    {
//        try {
//            return $this->createQueryBuilder('p')
//                ->setFirstResult($offset)
//                ->setMaxResults($limit)
//                ->orderBy('p.date', 'DESC')
//                ->getQuery()
//                ->getResult();
//        } catch (NonUniqueResultException $e) {
//            return null;
//        }
//        }

    /**
     * @return mixed
     */
    public function allPosts()
    {
        return $this->createQueryBuilder('p')
            ->where('p.date is not null')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Post[] Returns an array of Post objects
     */

    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }



    public function findByUserField($user)
    {
        try {
            return $this->createQueryBuilder('p')
                ->andWhere('p.user_id = :val')
                ->setParameter('val', $user)
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


}
