<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    private $newPass;

    /**
     * Sets the email.
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    /**
     * Set the canonical email.
     *
     * @param string $emailCanonical
     * @return User
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->setUsernameCanonical($emailCanonical);

        return parent::setEmailCanonical($emailCanonical);
    }



    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="user_id")
     */
    private $comment;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LikeDislike", mappedBy="user_id")
     */
    private $likeDislikes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Actuality", mappedBy="user_id")
     */
    private $actualities;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Satisfied", mappedBy="user_id")
     */
    private $satisfieds;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="user_id")
     */
    private $posts;


    public function __construct()
    {
        parent::__construct();
        $this->posts = new ArrayCollection();
        $this->comment = new ArrayCollection();
        $this->likeDislikes = new ArrayCollection();
        $this->actualities = new ArrayCollection();
        $this->satisfieds = new ArrayCollection();
    }

    public function getNewPass()
    {
        return $this->newPass;
    }

    /**
     * @param mixed $newPass
     * @return User
     */
    public function setNewPass($newPass)
    {
        $this->newPass = $newPass;
        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComment(): Collection
    {
        return $this->comment;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comment->contains($comment)) {
            $this->comment[] = $comment;
            $comment->setUserId($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comment->contains($comment)) {
            $this->comment->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getUserId() === $this) {
                $comment->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LikeDislike[]
     */
    public function getLikeDislikes(): Collection
    {
        return $this->likeDislikes;
    }

    public function addLikeDislike(LikeDislike $likeDislike): self
    {
        if (!$this->likeDislikes->contains($likeDislike)) {
            $this->likeDislikes[] = $likeDislike;
            $likeDislike->setUserId($this);
        }

        return $this;
    }

    public function removeLikeDislike(LikeDislike $likeDislike): self
    {
        if ($this->likeDislikes->contains($likeDislike)) {
            $this->likeDislikes->removeElement($likeDislike);
            // set the owning side to null (unless already changed)
            if ($likeDislike->getUserId() === $this) {
                $likeDislike->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Actuality[]
     */
    public function getActualities(): Collection
    {
        return $this->actualities;
    }

    public function addActuality(Actuality $actuality): self
    {
        if (!$this->actualities->contains($actuality)) {
            $this->actualities[] = $actuality;
            $actuality->setUserId($this);
        }

        return $this;
    }

    public function removeActuality(Actuality $actuality): self
    {
        if ($this->actualities->contains($actuality)) {
            $this->actualities->removeElement($actuality);
            // set the owning side to null (unless already changed)
            if ($actuality->getUserId() === $this) {
                $actuality->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Satisfied[]
     */
    public function getSatisfieds(): Collection
    {
        return $this->satisfieds;
    }

    public function addSatisfied(Satisfied $satisfied): self
    {
        if (!$this->satisfieds->contains($satisfied)) {
            $this->satisfieds[] = $satisfied;
            $satisfied->setUserId($this);
        }

        return $this;
    }

    public function removeSatisfied(Satisfied $satisfied): self
    {
        if ($this->satisfieds->contains($satisfied)) {
            $this->satisfieds->removeElement($satisfied);
            // set the owning side to null (unless already changed)
            if ($satisfied->getUserId() === $this) {
                $satisfied->setUserId(null);
            }
        }

        return $this;
    }

    public function addPosts(Post $post): self
    {
        if (!$this->satisfieds->contains($post)) {
            $this->posts[] = $post;
            $post->setUserId($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->satisfieds->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getUserId() === $this) {
                $post->setUserId(null);
            }
        }

        return $this;
    }


}