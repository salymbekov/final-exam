<?php

namespace App\Entity;

use App\Repository\ActualityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="posts")
     *
     */
    private $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="name")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", cascade={"persist", "remove"}, mappedBy="posts")
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="post_id")
     */
    private $comments;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LikeDislike", mappedBy="post_id")
     */
    private $likeDislikes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Actuality", mappedBy="post_id")
     */
    private $actualities;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Satisfied", mappedBy="post_id")
     */
    private $satisfieds;

    public function __toString()
    {
        return $this->title;
    }

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->createDate = new \DateTime();
        $this->likeDislikes = new ArrayCollection();
        $this->actualities = new ArrayCollection();
        $this->satisfieds = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->addPost($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removePost($this);
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setPostId($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getPostId() === $this) {
                $comment->setPostId(null);
            }
        }

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCreateDate(): ?\DateTimeInterface
    {
        return $this->createDate;
    }

    public function setCreateDate(\DateTimeInterface $createDate): self
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * @return Collection|LikeDislike[]
     */
    public function getLikeDislikes(): Collection
    {
        return $this->likeDislikes;
    }

    public function addLikeDislike(LikeDislike $likeDislike): self
    {
        if (!$this->likeDislikes->contains($likeDislike)) {
            $this->likeDislikes[] = $likeDislike;
            $likeDislike->setPostId($this);
        }

        return $this;
    }

    public function removeLikeDislike(LikeDislike $likeDislike): self
    {
        if ($this->likeDislikes->contains($likeDislike)) {
            $this->likeDislikes->removeElement($likeDislike);
            // set the owning side to null (unless already changed)
            if ($likeDislike->getPostId() === $this) {
                $likeDislike->setPostId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Actuality[]
     */
    public function getActualities(): Collection
    {
        return $this->actualities;
    }

    public function addActuality(Actuality $actuality): self
    {
        if (!$this->actualities->contains($actuality)) {
            $this->actualities[] = $actuality;
            $actuality->setPostId($this);
        }

        return $this;
    }

    public function removeActuality(Actuality $actuality): self
    {
        if ($this->actualities->contains($actuality)) {
            $this->actualities->removeElement($actuality);
            // set the owning side to null (unless already changed)
            if ($actuality->getPostId() === $this) {
                $actuality->setPostId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Satisfied[]
     */
    public function getSatisfieds(): Collection
    {
        return $this->satisfieds;
    }

    public function addSatisfied(Satisfied $satisfied): self
    {
        if (!$this->satisfieds->contains($satisfied)) {
            $this->satisfieds[] = $satisfied;
            $satisfied->setPostId($this);
        }

        return $this;
    }

    public function removeSatisfied(Satisfied $satisfied): self
    {
        if ($this->satisfieds->contains($satisfied)) {
            $this->satisfieds->removeElement($satisfied);
            // set the owning side to null (unless already changed)
            if ($satisfied->getPostId() === $this) {
                $satisfied->setPostId(null);
            }
        }

        return $this;
    }


}
