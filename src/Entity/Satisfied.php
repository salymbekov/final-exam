<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SatisfiedRepository")
 */
class Satisfied
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $satisfaction;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="satisfieds")
     */
    private $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Post", inversedBy="satisfieds")
     */
    private $post_id;

    public function getId()
    {
        return $this->id;
    }

    public function getSatisfaction(): ?int
    {
        return $this->satisfaction;
    }

    public function setSatisfaction(int $satisfaction): self
    {
        $this->satisfaction = $satisfaction;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->user_id;
    }

    public function setUserId(?User $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getPostId(): ?Post
    {
        return $this->post_id;
    }

    public function setPostId(?Post $post_id): self
    {
        $this->post_id = $post_id;

        return $this;
    }
}
