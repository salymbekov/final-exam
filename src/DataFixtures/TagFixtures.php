<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends Fixture
{

    public const TAG_REFERENCE = ['машина', 'холодец', 'чистый город', 'програмист', 'зима', 'мороз', 'столица'];

    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    ) {

        foreach (self::TAG_REFERENCE as $key => $value) {
            $tag = new Tag();
            $tag
                ->setName($value);
            $this->addReference($value, $tag);
            $manager->persist($tag);
        }

        $manager->flush();
    }
}
