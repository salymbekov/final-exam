<?php
/**
 * Created by PhpStorm.
 * User: timur
 * Date: 29.09.18
 * Time: 10:33
 */

namespace App\DataFixtures;


use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PostFixtures extends  Fixture implements DependentFixtureInterface
{

    public const POST_REFERENCE = [0, 1, 2, 3, 4, 5 ];
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i <= 2; $i++){
            $post = new Post();
            $post
                ->setTitle('Заголовок'.$i)
                ->setCreateDate(new \DateTime())
                ->setDescription('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, consequuntur corporis ducimus enim nobis numquam officia omnis, perspiciatis tenetur ullam vel velit. Ab cumque excepturi exercitationem, quae quo similique voluptatibus.')
                ->setUserId($this->getReference(UserFixtures::USER_REFERENCE[$i]))
                ->setCategory($this->getReference(CategoryFixtures::CATEGORIES[$i]))
                ->addTag($this->getReference(TagFixtures::TAG_REFERENCE[$i]))
                ->addTag($this->getReference(TagFixtures::TAG_REFERENCE[$i+1]));
            $manager->persist($post);
            $manager->flush();
        }

        for ($i = 0; $i <= 2; $i++){
            $post = new Post();
            $post
                ->setTitle($i.'Новость')
                ->setCreateDate(new \DateTime())
                ->setDescription('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, consequuntur corporis ducimus enim nobis numquam officia omnis, perspiciatis tenetur ullam vel velit. Ab cumque excepturi exercitationem, quae quo similique voluptatibus.')
                ->setUserId($this->getReference(UserFixtures::USER_REFERENCE[$i]))
                ->setCategory($this->getReference(CategoryFixtures::CATEGORIES[$i]))
                ->addTag($this->getReference(TagFixtures::TAG_REFERENCE[$i+1]))
                ->setDate(new \DateTime());
            $this->addReference($i, $post);
            $manager->persist($post);
            $manager->flush();
        }
        for ($i = 0; $i <= 2; $i++){
            $post = new Post();
            $post
                ->setTitle('Открытие'.$i)
                ->setCreateDate(new \DateTime())
                ->setDescription('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem, consequuntur corporis ducimus enim nobis numquam officia omnis, perspiciatis tenetur ullam vel velit. Ab cumque excepturi exercitationem, quae quo similique voluptatibus.')
                ->setUserId($this->getReference(UserFixtures::USER_REFERENCE[$i]))
                ->setCategory($this->getReference(CategoryFixtures::CATEGORIES[$i]))
                ->addTag($this->getReference(TagFixtures::TAG_REFERENCE[$i+2]))
                ->setDate(new \DateTime());
            $this->addReference($i+3, $post);
            $manager->persist($post);
            $manager->flush();
        }
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            CategoryFixtures::class,
            TagFixtures::class
        );
    }
}