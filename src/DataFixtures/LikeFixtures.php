<?php
/**
 * Created by PhpStorm.
 * User: timur
 * Date: 29.09.18
 * Time: 23:12
 */

namespace App\DataFixtures;


use App\Entity\Actuality;
use App\Entity\LikeDislike;
use App\Entity\Satisfied;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LikeFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            CategoryFixtures::class,
            TagFixtures::class,
            PostFixtures::class
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for($j = 0; $j < 2; $j++){
            for ($i=0; $i < 3; $i++){
                $like = new LikeDislike();
                $actual = new Actuality();
                $satisfied = new Satisfied();
                $like
                    ->setMark(1)
                    ->setPostId($this->getReference(PostFixtures::POST_REFERENCE[$i]))
                    ->setUserId($this->getReference(UserFixtures::USER_REFERENCE[$i]));
                $actual
                    ->setActual(-1)
                    ->setPostId($this->getReference(PostFixtures::POST_REFERENCE[$i]))
                    ->setUserId($this->getReference(UserFixtures::USER_REFERENCE[$i]));
                $satisfied
                    ->setSatisfaction(1)
                    ->setPostId($this->getReference(PostFixtures::POST_REFERENCE[$i]))
                    ->setUserId($this->getReference(UserFixtures::USER_REFERENCE[$i]));
                $manager->persist($like);
                $manager->persist($actual);
                $manager->persist($satisfied);
                $manager->flush();
            }

            for ($i=0; $i < 3; $i++){
                $like = new LikeDislike();
                $actual = new Actuality();
                $satisfied = new Satisfied();
                $like
                    ->setMark(-1)
                    ->setPostId($this->getReference(PostFixtures::POST_REFERENCE[$i+3]))
                    ->setUserId($this->getReference(UserFixtures::USER_REFERENCE[$i]));
                $actual
                    ->setActual(1)
                    ->setPostId($this->getReference(PostFixtures::POST_REFERENCE[$i+3]))
                    ->setUserId($this->getReference(UserFixtures::USER_REFERENCE[$i]));
                $satisfied
                    ->setSatisfaction(-1)
                    ->setPostId($this->getReference(PostFixtures::POST_REFERENCE[$i+3]))
                    ->setUserId($this->getReference(UserFixtures::USER_REFERENCE[$i]));

                $manager->persist($like);
                $manager->persist($actual);
                $manager->persist($satisfied);
                $manager->flush();
            }

        }


    }
}