<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Model\Enumeration\ClientTypeEnumeration;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    public const USER_REFERENCE = ['morty@gmail.com', 'mor@gmail.com', 'mort@gmail.com' ];
    /**
     * @param ObjectManager $manager
     */
    public function load
    (
        ObjectManager $manager
    ) {

        foreach (self::USER_REFERENCE as $key => $value) {
            $user = new User();
            $user
                ->setEmail($value)
                ->setPlainPassword('qwerty')
                ->setEnabled(true)
                ->setRoles(['ROLE_USER']);
            $this->addReference($value, $user);
            $manager->persist($user);
        }

        $admin = new User();
        $admin
            ->setEmail('admin@gmail.com')
            ->setPlainPassword('123')
            ->setEnabled(true)
            ->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);
        $manager->flush();
    }
}
