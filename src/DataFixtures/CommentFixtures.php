<?php
/**
 * Created by PhpStorm.
 * User: timur
 * Date: 29.09.18
 * Time: 10:33
 */

namespace App\DataFixtures;


use App\Entity\Comment;
use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CommentFixtures extends  Fixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i <= 2; $i++){
            $comment = new Comment();
            $comment
                ->setMessage('Lorem ipsum dolor sit amet, consectetur adipisicing.')
                ->setDate(new \DateTime())
                ->setUserId($this->getReference(UserFixtures::USER_REFERENCE[$i]))
                ->setPostId($this->getReference(PostFixtures::POST_REFERENCE[$i]));
            $manager->persist($comment);
            $manager->flush();
        }


    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            CategoryFixtures::class,
            PostFixtures::class
        );
    }
}