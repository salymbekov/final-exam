<?php


namespace App\Controller;


use App\Entity\Actuality;
use App\Entity\Comment;
use App\Entity\LikeDislike;
use App\Entity\Post;
use App\Entity\Satisfied;
use App\Entity\User;
use App\Form\CommentType;
use App\Repository\ActualityRepository;
use App\Repository\CategoryRepository;
use App\Repository\LikeDislikeRepository;
use App\Repository\LikeRepository;
use App\Repository\PostRepository;
use App\Repository\SatisfiedRepository;
use App\Repository\TagRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class IndexController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param PostRepository $postRepository
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @param EntityManager $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(
        Request $request,
        PostRepository $postRepository,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository
    ) {

        $tags = $tagRepository->findAll();

        $allPost = $postRepository->allPosts();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $allPost, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            3/*limit per page*/
        );
        $categories = $categoryRepository->findAll();
        return $this->render('index.html.twig', [
            'categories' => $categories,
            'tags' => $tags,
            'pages' => $pagination
        ]);
    }

    /**
     * @Route("/category/{id}", requirements={"id": "\d+"}, name="category_posts")
     * @param int $id
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postByCategoryAction(
        int $id,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository
    ) {
        $categories = $categoryRepository->findAll();
        $category = $categoryRepository->find($id);
        $posts = $category->getPosts();
        $tags = $tagRepository->findAll();
        return $this->render('category.html.twig', [
            'posts' => $posts,
            'categories' => $categories,
            'tags' => $tags
        ]);
    }


    /**
     * @Route("/tag/{id}", requirements={"id": "\d+"}, name="tag_posts")
     * @param int $id
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postByTagAction(
        int $id,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository
    ) {
        $categories = $categoryRepository->findAll();
        $tag = $tagRepository->find($id);
        $posts = $tag->getPosts();
        $tags = $tagRepository->findAll();
        return $this->render('category.html.twig', [
            'posts' => $posts,
            'categories' => $categories,
            'tags' => $tags
        ]);
    }

    /**
     * @Route("/post/{id}", requirements={"id": "\d+"}, name="one_post")
     * @param int $id
     * @param PostRepository $postRepository
     * @param CategoryRepository $categoryRepository
     * @param Request $request
     * @param ObjectManager $manager
     * @param ActualityRepository $actualityRepository
     * @param LikeDislikeRepository $likeDislikeRepository
     * @param SatisfiedRepository $satisfiedRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function onePostAction(
        int $id,
        PostRepository $postRepository,
        CategoryRepository $categoryRepository,
        Request $request,
        ObjectManager $manager,
    ActualityRepository $actualityRepository,
    LikeDislikeRepository $likeDislikeRepository,
    SatisfiedRepository $satisfiedRepository
    ) {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        $categories = $categoryRepository->findAll();
        $post = $postRepository->find($id);
        $comments = $post->getComments();
        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setPostId($post);
            $comment->setUserId($this->getUser());
            $comment->setDate(new \DateTime());
            $manager->persist($comment);
            $manager->flush();
            $this->redirect($request->server->get('HTTP_REFERER'));
        }
        $actuality = $actualityRepository->countField($id);
        $like = $likeDislikeRepository->countField($id);
        $happy = $satisfiedRepository->countField($id);
        dump($actuality);
        return $this->render('post.html.twig', [
            'post' => $post,
            'categories' => $categories,
            'comments' => $comments,
            'form' => $form->createView(),
            'actuality' => $actuality,
            'like' => $like,
            'happy' => $happy
        ]);
    }


    /**
     * @Route("/post/new", name="newPost")
     * @param Request $request
     * @param ObjectManager $manager
     * @param CategoryRepository $categoryRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newPostAction(Request $request, ObjectManager $manager, CategoryRepository $categoryRepository)
    {
        $post = new Post();
        $form = $this->createForm('App\Form\PostType', $post);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post->setUserId($this->getUser());
            $manager->persist($post);
            $manager->flush();
            return $this->redirectToRoute('homepage');
        }
        $categoryRepository = $categoryRepository->findAll();
        return $this->render('newPost.html.twig', [
            'form' => $form->createView(),
            'categories'=> $categoryRepository
        ]);
    }

    /**
     * @Route("/mark/{post}/{type}/{mark}", name="markPost")
     * @param ObjectManager $manager
     * @param $type
     * @param $mark
     * @param $post
     * @param LikeDislikeRepository $likeDislikeRepository
     * @param SatisfiedRepository $satisfiedRepository
     * @param ActualityRepository $actualityRepository
     * @param PostRepository $postRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function likeAction(
        ObjectManager $manager,
        $type,
        $mark,
        $post,
        LikeDislikeRepository $likeDislikeRepository,
        SatisfiedRepository $satisfiedRepository,
        ActualityRepository $actualityRepository,
        PostRepository $postRepository,
        Request $request
    ) {
        switch ($type) {
            case 'l':
                $result = $likeDislikeRepository->findByUserField($this->getUser(), $postRepository->find($post));
                if (empty($result)) {
                    $like = new LikeDislike();
                    $like->setUserId($this->getUser())
                        ->setPostId($postRepository->find($post))
                        ->setMark($mark);
                    $manager->persist($like);
                    $manager->flush();
                } else {
                    $this->addFlash('warning', 'вы уже проголосовали');
                }
                break;
            case 's':
                $result = $satisfiedRepository->findByUserField($this->getUser(), $postRepository->find($post));
                if (empty($result)) {
                    $like = new Satisfied();
                    $like->setUserId($this->getUser())
                        ->setPostId($postRepository->find($post))
                        ->setSatisfaction($mark);
                    $manager->persist($like);
                    $manager->flush();
                } else {
                    $this->addFlash('warning', 'вы уже проголосовали');
                }
                break;
            case 'a':
                $result = $actualityRepository->findByUserField($this->getUser(), $postRepository->find($post));
                if (empty($result)) {
                    $like = new Actuality();
                    $like->setUserId($this->getUser())
                        ->setPostId($postRepository->find($post))
                        ->setActual($mark);
                    $manager->persist($like);
                    $manager->flush();
                } else {
                    $this->addFlash('warning', 'вы уже проголосовали');
                }
                break;

        }
        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * @Route("/profile/", name="profile")
     *
     * @param CategoryRepository $categoryRepository
     * @param PostRepository $postRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction(CategoryRepository $categoryRepository,
        PostRepository $postRepository, LikeDislikeRepository $likeDislikeRepository,
        ActualityRepository $actualityRepository, SatisfiedRepository $satisfiedRepository) {

        $user = $this->getUser();

        $userPosts = $postRepository->findByUserField($user);
        $userPosts_count = count($userPosts);
        $like = [];
        foreach ($userPosts as $value){
            $like[] = $likeDislikeRepository->countField($value);
            $like[] = $actualityRepository->countField($value);
        }
        $rating = ($userPosts_count + array_sum($like)) / 3;

        return $this->render('profile.html.twig', [
            'user' => $user,
            'categories' => $categoryRepository,
            'rating' => $rating
        ]);
    }

}