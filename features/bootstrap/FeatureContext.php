<?php

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{

    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     * @param $arg
     */
    public function ISeeTheWordWhereThatIsOnPage($arg)
    {
        $this->assertPageContainsText($arg);
        sleep(2);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function ImOnTheMainPage()
    {
        $this->visit($this->getContainer()->get('router')->generate('homepage'));
    }

    /**
     * @When /^я заполняю форму регистрации и регистрируюсь$/
     */
    public function IFillInTheRegistrationFormAndRegister()
    {
        $this->fillField('fos_user_registration_form_email', "test@gmail.com");
        $this->fillField('fos_user_registration_form_plainPassword_first', "test123!@#");
        $this->fillField('fos_user_registration_form_plainPassword_second', "test123!@#");


        $this->pressButton('Зарегистрироваться');
    }


    /**
     * @When /^я авторизуюсь с логином "([^"]*)" и паролем "([^"]*)"/
     * @param $login
     * @param $password
     */
    public function ImLoggedInWithALoginAndPassword($login, $password)
    {
        $this->fillField('username', $login);
        $this->fillField('password', $password);
        $this->pressButton('Войти');
    }

    /**
     * @When /^я кликаю по ссылке с id "([^"]*)"/
     * @param $link
     */
    public function iClickOnTheText($link)
    {
        $this->getSession()->getPage()->find('css', '#' . $link)->click();

    }
}

