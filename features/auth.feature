# language: ru
Функционал: Тестируем регистрацию и авторизацию

  Сценарий: Я пытаюсь зарегистрироваться
    Допустим я нахожусь на главной странице
    И я вижу слово "Регистрация" где-то на странице
    И я кликаю по ссылке с id "app_register"
    И я заполняю форму регистрации и регистрируюсь
    И я вижу слово "Поздравляем" где-то на странице

  Сценарий: Я пытаюсь зарегистрироваться
    Допустим я нахожусь на главной странице
    И я кликаю по ссылке с id "app_login"
    И я авторизуюсь с логином "morty@gmail.com" и паролем "qwerty"
    И я вижу слово "Выйти" где-то на странице